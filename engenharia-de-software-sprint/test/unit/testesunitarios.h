#ifndef TESTESUNITARIOS_H
#define TESTESUNITARIOS_H


#include <iostream>
#include <string>
#include <assert.h>
#include <math.h>

#include "../../src/lib/system.h"
#include "../../src/lib/vehicle.h"
#include "../../src/lib/user.h"
#include "../../src/lib/rent.h"

void testeVehicle();

void testeUser();

void testeRent();

void testeSystem();

#endif // TESTESUNITARIOS_H
