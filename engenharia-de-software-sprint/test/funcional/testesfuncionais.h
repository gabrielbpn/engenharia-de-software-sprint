#ifndef TESTESFUNCIONAIS_H
#define TESTESFUNCIONAIS_H

#include <iostream>
#include <string>
#include <assert.h>
#include <math.h>

#include "../../src/lib/system.h"
#include "../../src/lib/vehicle.h"
#include "../../src/lib/user.h"
#include "../../src/lib/rent.h"


void testesFuncionais();


#endif // TESTESFUNCIONAIS_H
