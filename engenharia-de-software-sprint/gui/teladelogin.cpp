#include "teladelogin.h"
#include "ui_teladelogin.h"

TelaDeLogin::TelaDeLogin(QWidget *parent)
    : QMainWindow(parent)
    , ui(new Ui::TelaDeLogin)
{
    ui->setupUi(this);
    system = System::criateSystem("system");

    user = system->criateUser("Lucas");
    user->setPassword("123");
    user->setStreet("Felipe dos Santo,110");
    user->setCity("Ouro Preto");
    user->setDistrict("Antônio Dias");
    user->setState("Minas Gerais");
    user->setCnh("cnh");
    user->setCpf("12212212233");
    user->setEmail("lucas@email.com");
    user->setPhone("99999");
}

TelaDeLogin::~TelaDeLogin()
{
    delete ui;
}


void TelaDeLogin::on_pushButton_clicked()
{
    if(ui->login->text().toStdString() == user->getCpf() && ui->senha->text().toStdString() == user->getPassword()){
        main = new MainWindow(this,system,user);
        main->setVisible(true);
    }
    else{
        QMessageBox qmsg;
        qmsg.setText("Login ou senha incorretos ");
        qmsg.exec();
    }
}

