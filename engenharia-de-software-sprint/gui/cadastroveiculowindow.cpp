#include "cadastroveiculowindow.h"
#include "ui_cadastroveiculowindow.h"

CadastroVeiculoWindow::CadastroVeiculoWindow(QWidget *parent, System* system, User* user) :
    QMainWindow(parent),
    ui(new Ui::CadastroVeiculoWindow)
{
    ui->setupUi(this);
    sys = system;
    us = user;
}

CadastroVeiculoWindow::~CadastroVeiculoWindow()
{
    delete ui;
}

void CadastroVeiculoWindow::on_cancelarButton_clicked()
{
    this->close();
}


void CadastroVeiculoWindow::on_cadastrarButton_clicked()
{
    aluguel = sys->criateRent(us);

    aluguel->setVehicle(sys->criateVehicle(ui->modeloText->text().toStdString(),
                                           ui->marcaText->text().toStdString(),
                                           ui->precoText->text().toDouble(),
                                           ui->descricaoText->toPlainText().toStdString(),
                                           ui->anoText->text().toInt()));
    aluguel->getVehicle()->setImage(ui->caminhoImagem->text().toStdString());

//    QMessageBox qmsg;
//    qmsg.setText(QString::fromStdString(sys->getRent(1)->getVehicle()->getDescription()));
//    qmsg.exec();


//    vector<Rent*> rent = sys->getRent(ui->modeloText->text().toStdString());
//    Vehicle* car = rent[0]->getVehicle();


    this->close();
}

void CadastroVeiculoWindow::on_imagemButton_clicked()
{
    if(ui->caminhoImagem->text() != ""){
        QPixmap img(ui->caminhoImagem->text());
        ui->imagemLabel->setPixmap(img.scaled(191,151,Qt::KeepAspectRatio));
    }
}

