#include "meusveiculoswindow.h"
#include "ui_meusveiculoswindow.h"

MeusVeiculosWindow::MeusVeiculosWindow(QWidget *parent,User* us,System* sys) :
    QMainWindow(parent),
    ui(new Ui::MeusVeiculosWindow)
{
    ui->setupUi(this);
    system = sys;
    user = us;

    rents = sys->getRent(user);

    if(rents.size()){
        ui->descricao->setText(QString::fromStdString(rents[0]->getVehicle()->getModel())+"/"+ QString::fromStdString(rents[0]->getVehicle()->getBrand())+" ano "+QString::number(rents[0]->getVehicle()->getYear()));
        QPixmap img(QString::fromStdString(rents[0]->getVehicle()->getImage()));
        ui->imagem->setPixmap(img.scaled(191,151,Qt::KeepAspectRatio));
        if(rents.size() >= 2){
            ui->descricao_3->setText(QString::fromStdString(rents[1]->getVehicle()->getModel())+"/"+ QString::fromStdString(rents[1]->getVehicle()->getBrand())+" ano "+QString::number(rents[1]->getVehicle()->getYear()));
            QPixmap img2(QString::fromStdString(rents[1]->getVehicle()->getImage()));
            ui->imagem_3->setPixmap(img2.scaled(191,151,Qt::KeepAspectRatio));
        }
        else{
            ui->descricao_3->setText("");
            QPixmap img2(QString::fromStdString(""));
            ui->imagem_3->setPixmap(img2.scaled(191,151,Qt::KeepAspectRatio));
        }
        if(rents.size()>= 3){
            ui->descricao_5->setText(QString::fromStdString(rents[2]->getVehicle()->getModel())+"/"+ QString::fromStdString(rents[2]->getVehicle()->getBrand())+" ano "+QString::number(rents[2]->getVehicle()->getYear()));
            QPixmap img3(QString::fromStdString(rents[2]->getVehicle()->getImage()));
            ui->imagem_5->setPixmap(img3.scaled(191,151,Qt::KeepAspectRatio));
        }
        else{
            ui->descricao_5->setText("");
            QPixmap img3(QString::fromStdString(""));
            ui->imagem_5->setPixmap(img3.scaled(191,151,Qt::KeepAspectRatio));
        }
        if(rents.size()>= 4){
            ui->descricao_4->setText(QString::fromStdString(rents[3]->getVehicle()->getModel())+"/"+ QString::fromStdString(rents[3]->getVehicle()->getBrand())+" ano "+QString::number(rents[3]->getVehicle()->getYear()));
            QPixmap img4(QString::fromStdString(rents[3]->getVehicle()->getImage()));
            ui->imagem_4->setPixmap(img4.scaled(191,151,Qt::KeepAspectRatio));
        }
        else{
            ui->descricao_4->setText("");
            QPixmap img4(QString::fromStdString(""));
            ui->imagem_4->setPixmap(img4.scaled(191,151,Qt::KeepAspectRatio));
        }
    }
}

MeusVeiculosWindow::~MeusVeiculosWindow()
{
    delete ui;
}
