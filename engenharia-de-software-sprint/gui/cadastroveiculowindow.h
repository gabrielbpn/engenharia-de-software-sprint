#ifndef CADASTROVEICULOWINDOW_H
#define CADASTROVEICULOWINDOW_H

#include <QMainWindow>
#include <QDialog>
#include <QMessageBox>
#include <QPixmap>
#include "../src/lib/system.h"

namespace Ui {
class CadastroVeiculoWindow;
}

class CadastroVeiculoWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit CadastroVeiculoWindow(QWidget *parent = nullptr, System* system=nullptr, User* user=nullptr);
    ~CadastroVeiculoWindow();

private slots:
    void on_cancelarButton_clicked();

    void on_cadastrarButton_clicked();

    void on_imagemButton_clicked();

private:
    System* sys;
    Rent* aluguel;
    User* us;
    Ui::CadastroVeiculoWindow *ui;
};

#endif // CADASTROVEICULOWINDOW_H
