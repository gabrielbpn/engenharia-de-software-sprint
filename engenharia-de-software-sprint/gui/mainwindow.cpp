#include "mainwindow.h"
#include "ui_mainwindow.h"

MainWindow::MainWindow(QWidget *parent, System* system, User* us)
    : QMainWindow(parent)
    , ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    sys = system;
    user = us;
    carregarDados();
    rents = sys->getRent(pos);
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::carregarDados(){

    car = sys->criateVehicle("Gol","Volkswagen",8000,"Carro completo, vidro, trava, ar condicionado e direção hidraulica",1999);
    sys->criateRent(user)->setVehicle(car);
    car->setImage("C:/Users/Lucas de Souza Silva/Documents/UFOP/Eng 1/tp2/engenharia-de-software-sprint/engenharia-de-software-sprint/img/gol.jpg");
    ui->descricao->setText(QString::fromStdString(car->getModel())+"/"+ QString::fromStdString(car->getBrand())+" ano "+QString::number(car->getYear()));
    QPixmap img(QString::fromStdString(car->getImage()));
    ui->imagem->setPixmap(img.scaled(191,151,Qt::KeepAspectRatio));

    car = sys->criateVehicle("Polo","Volkswagen",100000,"Carro completo, vidro, trava, ar condicionado e direção hidraulica",2021);
    sys->criateRent(user)->setVehicle(car);
    car->setImage("C:/Users/Lucas de Souza Silva/Documents/UFOP/Eng 1/tp2/engenharia-de-software-sprint/engenharia-de-software-sprint/img/polo.jpg");
    ui->descricao_3->setText(QString::fromStdString(car->getModel())+"/"+ QString::fromStdString(car->getBrand())+" ano "+QString::number(car->getYear()));
    QPixmap img2(QString::fromStdString(car->getImage()));
    ui->imagem_3->setPixmap(img2.scaled(191,151,Qt::KeepAspectRatio));

    car = sys->criateVehicle("Uno","Fiat",8000,"Carro completo, vidro, trava, ar condicionado e direção hidraulica",1999);
    sys->criateRent(user)->setVehicle(car);
    car->setImage("C:/Users/Lucas de Souza Silva/Documents/UFOP/Eng 1/tp2/engenharia-de-software-sprint/engenharia-de-software-sprint/img/uno.jpg");
    ui->descricao_5->setText(QString::fromStdString(car->getModel())+"/"+ QString::fromStdString(car->getBrand())+" ano "+QString::number(car->getYear()));
    QPixmap img3(QString::fromStdString(car->getImage()));
    ui->imagem_5->setPixmap(img3.scaled(191,151,Qt::KeepAspectRatio));

    car = sys->criateVehicle("Golf","Volkswagen",8000,"Carro completo, vidro, trava, ar condicionado e direção hidraulica",1997);
    sys->criateRent(user)->setVehicle(car);
    car->setImage("C:/Users/Lucas de Souza Silva/Documents/UFOP/Eng 1/tp2/engenharia-de-software-sprint/engenharia-de-software-sprint/img/golf.jpg");
    ui->descricao_4->setText(QString::fromStdString(car->getModel())+"/"+ QString::fromStdString(car->getBrand())+" ano "+QString::number(car->getYear()));
    QPixmap img4(QString::fromStdString(car->getImage()));
    ui->imagem_4->setPixmap(img4.scaled(191,151,Qt::KeepAspectRatio));

    ui->nomeUser->setText(QString::fromStdString(user->getName()));

    QPixmap img5("C:/Users/Lucas de Souza Silva/Documents/UFOP/Eng 1/tp2/engenharia-de-software-sprint/engenharia-de-software-sprint/img/user.png");
    ui->imagemUser->setPixmap(img5.scaled(51,41,Qt::KeepAspectRatio));

}

bool MainWindow::atualizarPagina(){

    if(rents.size()){
        ui->descricao->setText(QString::fromStdString(rents[0]->getVehicle()->getModel())+"/"+ QString::fromStdString(rents[0]->getVehicle()->getBrand())+" ano "+QString::number(rents[0]->getVehicle()->getYear()));
        QPixmap img(QString::fromStdString(rents[0]->getVehicle()->getImage()));
        ui->imagem->setPixmap(img.scaled(191,151,Qt::KeepAspectRatio));
        if(rents.size() >= 2){
            ui->descricao_3->setText(QString::fromStdString(rents[1]->getVehicle()->getModel())+"/"+ QString::fromStdString(rents[1]->getVehicle()->getBrand())+" ano "+QString::number(rents[1]->getVehicle()->getYear()));
            QPixmap img2(QString::fromStdString(rents[1]->getVehicle()->getImage()));
            ui->imagem_3->setPixmap(img2.scaled(191,151,Qt::KeepAspectRatio));
        }
        else{
            ui->descricao_3->setText("");
            QPixmap img2(QString::fromStdString(""));
            ui->imagem_3->setPixmap(img2.scaled(191,151,Qt::KeepAspectRatio));
        }
        if(rents.size()>= 3){
            ui->descricao_5->setText(QString::fromStdString(rents[2]->getVehicle()->getModel())+"/"+ QString::fromStdString(rents[2]->getVehicle()->getBrand())+" ano "+QString::number(rents[2]->getVehicle()->getYear()));
            QPixmap img3(QString::fromStdString(rents[2]->getVehicle()->getImage()));
            ui->imagem_5->setPixmap(img3.scaled(191,151,Qt::KeepAspectRatio));
        }
        else{
            ui->descricao_5->setText("");
            QPixmap img3(QString::fromStdString(""));
            ui->imagem_5->setPixmap(img3.scaled(191,151,Qt::KeepAspectRatio));
        }
        if(rents.size()>= 4){
            ui->descricao_4->setText(QString::fromStdString(rents[3]->getVehicle()->getModel())+"/"+ QString::fromStdString(rents[3]->getVehicle()->getBrand())+" ano "+QString::number(rents[3]->getVehicle()->getYear()));
            QPixmap img4(QString::fromStdString(rents[3]->getVehicle()->getImage()));
            ui->imagem_4->setPixmap(img4.scaled(191,151,Qt::KeepAspectRatio));
        }
        else{
            ui->descricao_4->setText("");
            QPixmap img4(QString::fromStdString(""));
            ui->imagem_4->setPixmap(img4.scaled(191,151,Qt::KeepAspectRatio));
        }
        return true;
    }
    else
        return false;
}


void MainWindow::on_adicionarButton_clicked()
{
    cadastro = new CadastroVeiculoWindow(this,sys,user);
    cadastro->show();
    rents = sys->getRent(0);
    atualizarPagina();
    pos=0;
}


void MainWindow::on_editarButton_clicked()
{
    editar = new EditarPerfilWindow(this,user);
    editar->show();
}

void MainWindow::on_maisButton_clicked(){
    car = rents[0]->getVehicle();
    QMessageBox qmsg;
    qmsg.setWindowTitle("Descrição");
    qmsg.setText("Marca: "+QString::fromStdString(car->getBrand())+"\nModelo: "+QString::fromStdString(car->getModel())+"\nAno: "+QString::number(car->getYear())+"\nPreço: "+QString::number(car->getPrice())+"\nDescrição: "+QString::fromStdString(car->getDescription()));
    qmsg.exec();
}

void MainWindow::on_pesquisarButton_clicked()
{
    if(!ui->pesquisaText->text().toStdString().size()){
        QMessageBox qmsg;
        qmsg.setText("Digite o modelo do veículo para fazer a busca !");
        qmsg.exec();
    }
    else{
        rents = sys->getRent(ui->pesquisaText->text().toStdString());
        if(rents.size()==0){
            QMessageBox qmsg;
            qmsg.setText("Nenhum veículo encontrado !");
            qmsg.exec();
        }
        atualizarPagina();
    }
}


void MainWindow::on_paginaButton_clicked()
{
    pos+=4;
    rents = sys->getRent(pos);
    if(atualizarPagina()){}
    else
        pos=0;
}


void MainWindow::on_paginaButtonBack_clicked()
{
    if(pos>=4)
        pos-=4;
    else
        pos=0;
    rents = sys->getRent(pos);
    if(atualizarPagina()){}
    else
        pos=0;
}


void MainWindow::on_alugarButton_clicked()
{
    alugar = new AlugarDialog(this,rents[0]);
    alugar->show();
    alugar->exec();
    pos=0;
    rents = sys->getRent(pos);
    atualizarPagina();
}


void MainWindow::on_alugarButton_3_clicked()
{
    alugar = new AlugarDialog(this,rents[1]);
    alugar->show();
    alugar->exec();
    pos=0;
    rents = sys->getRent(pos);
    atualizarPagina();
}


void MainWindow::on_alugarButton_5_clicked()
{
    alugar = new AlugarDialog(this,rents[2]);
    alugar->show();
    alugar->exec();
    pos=0;
    rents = sys->getRent(pos);
    atualizarPagina();
}


void MainWindow::on_alugarButton_4_clicked()
{
    alugar = new AlugarDialog(this,rents[3]);
    alugar->show();
    alugar->exec();
    pos=0;
    rents = sys->getRent(pos);
    atualizarPagina();
}


void MainWindow::on_maisButton_3_clicked()
{
    car = rents[1]->getVehicle();
    QMessageBox qmsg;
    qmsg.setWindowTitle("Descrição");
    qmsg.setText("Marca: "+QString::fromStdString(car->getBrand())+"\nModelo: "+QString::fromStdString(car->getModel())+"\nAno: "+QString::number(car->getYear())+"\nPreço: "+QString::number(car->getPrice())+"\nDescrição: "+QString::fromStdString(car->getDescription()));
    qmsg.exec();
}


void MainWindow::on_maisButton_5_clicked()
{
    car = rents[2]->getVehicle();
    QMessageBox qmsg;
    qmsg.setWindowTitle("Descrição");
    qmsg.setText("Marca: "+QString::fromStdString(car->getBrand())+"\nModelo: "+QString::fromStdString(car->getModel())+"\nAno: "+QString::number(car->getYear())+"\nPreço: "+QString::number(car->getPrice())+"\nDescrição: "+QString::fromStdString(car->getDescription()));
    qmsg.exec();
}


void MainWindow::on_maisButton_4_clicked()
{
    car = rents[3]->getVehicle();
    QMessageBox qmsg;
    qmsg.setWindowTitle("Descrição");
    qmsg.setText("Marca: "+QString::fromStdString(car->getBrand())+"\nModelo: "+QString::fromStdString(car->getModel())+"\nAno: "+QString::number(car->getYear())+"\nPreço: "+QString::number(car->getPrice())+"\nDescrição: "+QString::fromStdString(car->getDescription()));
    qmsg.exec();
}


void MainWindow::on_veiculosButton_clicked()
{
    veiculos = new MeusVeiculosWindow(this,user,sys);
    veiculos->show();
}

