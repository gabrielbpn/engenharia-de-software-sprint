#include "editarperfilwindow.h"
#include "ui_editarperfilwindow.h"

EditarPerfilWindow::EditarPerfilWindow(QWidget *parent, User* user) :
    QMainWindow(parent),
    ui(new Ui::EditarPerfilWindow)
{
    ui->setupUi(this);
    us = user;

    QPixmap img("C:/Users/Lucas de Souza Silva/Documents/UFOP/Eng 1/tp2/engenharia-de-software-sprint/engenharia-de-software-sprint/img/user.png");
    ui->fotoPerfil->setPixmap(img.scaled(51,41,Qt::KeepAspectRatio));
    ui->nome->setText(QString::fromStdString(us->getName()));

    ui->bairroText->setText(QString::fromStdString(us->getDistrict()));
    ui->cidadeText->setText(QString::fromStdString(us->getCity()));
    ui->estadoText->setText(QString::fromStdString(us->getState()));
    ui->ruaText->setText(QString::fromStdString(us->getStreet()));
    ui->cpfText->setText(QString::fromStdString(us->getCpf()));
    ui->emailText->setText(QString::fromStdString(us->getEmail()));
    ui->nomeText->setText(QString::fromStdString(us->getName()));
    ui->telefoneText->setText(QString::fromStdString(us->getPhone()));
    ui->nascText->setText(QString::fromStdString("15/09/1998"));
}

EditarPerfilWindow::~EditarPerfilWindow()
{
    delete ui;
}

void EditarPerfilWindow::on_salvarButton_clicked()
{
    us->setDistrict(ui->bairroText->text().toStdString());
    us->setCity(ui->cidadeText->text().toStdString());
    us->setState(ui->estadoText->text().toStdString());
    us->setStreet(ui->ruaText->text().toStdString());
    us->setCpf(ui->cpfText->text().toStdString());
    us->setEmail(ui->emailText->text().toStdString());
    us->setName(ui->nomeText->text().toStdString());
    us->setPhone(ui->telefoneText->text().toStdString());
    this->close();
}

