#include "alugardialog.h"
#include "ui_alugardialog.h"

AlugarDialog::AlugarDialog(QWidget *parent, Rent* rent) :
    QDialog(parent),
    ui(new Ui::AlugarDialog)
{
    ui->setupUi(this);

    rentAux = rent;
    ui->diariaLabel->setText("Valor da diária: "+QString::number(rentAux->getVehicle()->getPrice()));
}

AlugarDialog::~AlugarDialog()
{
    delete ui;
}

void AlugarDialog::on_calcularButton_clicked()
{
    ui->valorFinalLabel->setText("Valor final: "+QString::number(rentAux->getVehicle()->getPrice()*ui->diasText->text().toInt()));
}


void AlugarDialog::on_alugarButton_clicked()
{
    if(ui->diasText->text().toStdString()!="" && ui->diasText->text().toInt()>0){
        rentAux->setCost(rentAux->getVehicle()->getPrice()*ui->diasText->text().toInt());
        rentAux->setStatus(0);
        this->close();
    }
}

