#ifndef MEUSVEICULOSWINDOW_H
#define MEUSVEICULOSWINDOW_H

#include <QMainWindow>
#include "../src/lib/system.h"

namespace Ui {
class MeusVeiculosWindow;
}

class MeusVeiculosWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MeusVeiculosWindow(QWidget *parent = nullptr, User* us = nullptr, System* sys = nullptr);
    ~MeusVeiculosWindow();

private:
    System* system;
    User* user;
    vector<Rent*> rents;
    Ui::MeusVeiculosWindow *ui;
};

#endif // MEUSVEICULOSWINDOW_H
