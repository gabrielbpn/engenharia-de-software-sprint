#ifndef TELADELOGIN_H
#define TELADELOGIN_H

#include <QMainWindow>
#include "mainwindow.h"

QT_BEGIN_NAMESPACE
namespace Ui { class TelaDeLogin; }
QT_END_NAMESPACE

class TelaDeLogin : public QMainWindow
{
    Q_OBJECT

public:
    TelaDeLogin(QWidget *parent = nullptr);
    ~TelaDeLogin();

private slots:
    void on_pushButton_clicked();

private:
    Ui::TelaDeLogin *ui;
    System* system;
    User* user;
    MainWindow* main;
};
#endif // TELADELOGIN_H
