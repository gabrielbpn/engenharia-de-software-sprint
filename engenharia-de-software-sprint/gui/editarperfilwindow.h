#ifndef EDITARPERFILWINDOW_H
#define EDITARPERFILWINDOW_H

#include <QMainWindow>
#include "../src/lib/system.h"

namespace Ui {
class EditarPerfilWindow;
}

class EditarPerfilWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit EditarPerfilWindow(QWidget *parent = nullptr, User* = nullptr);
    ~EditarPerfilWindow();

private slots:
    void on_salvarButton_clicked();

private:
    System* sys;
    User* us;
    Ui::EditarPerfilWindow *ui;
};

#endif // EDITARPERFILWINDOW_H
