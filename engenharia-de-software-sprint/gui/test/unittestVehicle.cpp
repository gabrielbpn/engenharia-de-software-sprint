﻿#include <QtTest/QtTest>
#include <QtDebug>
#include <QMessageBox>
#include <QTimer>
#include <QPushButton>
#include <QRadioButton>

#include "../lib/vehicle.h"
#include "../lib/vehicleHB.h"
#include "../gui/cadastroveiculowindow.h"

// TESTE UNITARIO COMPORTAMENTAL( funcionalidade + transições)
class TestRegisterVehicleGUI: public QObject
{
    Q_OBJECT

    private slots:
        void Usecase_data();
        void Usecase();

          void timeOut();

        private:
            Register_GUI d;

        };
          void TestRegisterGUI::Usecase_data(){

              // ENTRADA
              QTest::addColumn<QString>("marca");
              QTest::addColumn<QString>("modelo");
              QTest::addColumn<QString>("ano");
              QTest::addColumn<QString>("descricao");
              QTest::addColumn<QDouble>("preco");
              QTest::addColumn<QPushButton*>("Cadastrar");
              // SAIDA
              QTest::addColumn<QPushButton>("Cancelar");


                 QFETCH(double, preco);
                  QFETCH(QString, ano);
                 QFETCH(QRadioButton*, botao_operacao);
                 QFETCH(QString, resultado);

                 m.precoText->setText("");
                 m.anoText->setText("");
                 m.modeloText->setText("");
                 m.marcaText->setText("");

                 QTest::keyClicks(m.precoText, preco);
                 QTest::keyClicks(m.anoText, ano);
                 QTest::keyClicks(m.modeloText, modelo);
                 QTest::keyClicks(m.marcaText, marca);



              QVERIFY(!buffer.isNull());
          }
