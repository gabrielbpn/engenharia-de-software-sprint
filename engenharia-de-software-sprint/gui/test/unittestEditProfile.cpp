#include <QMessageBox>
#include <QTimer>
#include <QPushButton>
#include <QRadioButton>

#include "../lib/vehicle.h"
#include "../lib/vehicleHB.h"
#include "../gui/editarperfilwindow.h"

// TESTE UNITARIO COMPORTAMENTAL
class unittestEditProfile: public QObject
{
    Q_OBJECT

    private slots:
        void Usecase_data();
        void Usecase();

          void timeOut();

        private:
            Register_GUI d;

        };
        void TestMainGUI::Usecase_data(){

          //ENTRADA
          QTest::addColumn<QString>("nome");
          QTest::addColumn<double>("data");
          QTest::addColumn<int>("cpf");
          QTest::addColumn<QString>("rua");
          QTest::addColumn<QString>("cidade");
          QTest::addColumn<QString>("bairro");
          QTest::addColumn<QString>("estado");
          QTest::addColumn<QString>("email");
          QTest::addColumn<int>("telefone");
          QTest::addColumn<QPushButton*>("Salvar");



          QFETCH(QString, nome);
          QFETCH(double, data);
          QFETCH(int, cpf);
          QFETCH(QRadioButton*, botao_operacao);
          QFETCH(QString, rua);
          QFETCH(QString, cidade);
          QFETCH(QString, bairro);
          QFETCH(QString,estado);
          QFETCH(QString,email);
          QFETCH(int, telefone);

          m.nomeText->setText("");
          m.cpfText->setText("");
          m.ruaText->setText("");
          m.cidadeText->setText("");
          m.bairroText->setText("");
          m.estadoText->setText("");
          m.emailText->setText("");
          m.telefoneText->setText("");

        }
