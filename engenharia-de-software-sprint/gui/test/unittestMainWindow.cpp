﻿#include <QtTest/QtTest>
#include <QtDebug>
#include <QMessageBox>
#include <QTimer>
#include <QPushButton>
#include <QRadioButton>

#include "../lib/vehicle.h"
#include "../lib/vehicleHB.h"
#include "../gui/mainwindow.h"

// TESTE UNITARIO COMPORTAMENTAL( funcionalidade + transições)
class TestMainWindow: public QObject
{
    Q_OBJECT

    private slots:
        void Usecase_data();
        void Usecase();

          void timeOut();

        private:
            Register_GUI d;

        };
        void TestMainGUI::Usecase_data(){
          // Inside the Window
          QTest::addColumn<QPushButton*>("Alugar");
          QTest::addColumn<QPushButton*>("Mais");

          // Outside the Window
          QTest::addColumn<QPushButton*>("Editar dados");
          QTest::addColumn<QPushButton*>("Meus veiculos");
          QTest::addColumn<QPushButton*>("Adicionar veiculos");
          QTest::addColumn<QPushButton*>("Avancar");
          QTest::addColumn<QPushButton*>("Pesquisar");
        }


        void TestMainGUI::Usecase(){
          QTest::mouseClick(d.ui->editarButton, Qt::LeftButton);
          QTest::mouseClick(d.ui->veiculosButton, Qt::LeftButton);
          QTest::mouseClick(d.ui->adicionarButton, Qt::LeftButton);
          QTest::mouseClick(d.ui->maisButton, Qt::LeftButton);



        }
