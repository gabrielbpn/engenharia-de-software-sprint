#include <iostream>
#include "unittestVehicle.h"
#include "unittestMainWindow.h"
#include "unittestEditProfile.h"
using namespace std;


int main(){

  cout << "\nBegin of the Unit Tests";

  unittestVehicle();
  unittestMainWindow();
  unittestEditProfile();

  return 0;
}
