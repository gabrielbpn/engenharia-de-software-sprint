#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QtSql>
#include <QtDebug>
#include <QFileInfo>
#include"cadastroveiculowindow.h"
#include "editarperfilwindow.h"
#include "meusveiculoswindow.h"
#include "alugardialog.h"
#include "../src/lib/system.h"

QT_BEGIN_NAMESPACE
namespace Ui { class MainWindow; }
QT_END_NAMESPACE

class MainWindow : public QMainWindow
{
    Q_OBJECT
public:
    MainWindow(QWidget *parent = nullptr, System* system=nullptr,User* us=nullptr);
    void carregarDados();
    bool atualizarPagina();
    ~MainWindow();

private slots:
    void on_adicionarButton_clicked();

    void on_editarButton_clicked();

    void on_maisButton_clicked();

    void on_pesquisarButton_clicked();

    void on_paginaButton_clicked();

    void on_paginaButtonBack_clicked();

    void on_alugarButton_clicked();

    void on_alugarButton_3_clicked();

    void on_alugarButton_5_clicked();

    void on_alugarButton_4_clicked();

    void on_maisButton_3_clicked();

    void on_maisButton_5_clicked();

    void on_maisButton_4_clicked();

    void on_veiculosButton_clicked();

private:
    int pos = 0;
    System* sys;
    User* user;
    vector<Rent*> rents;
    vector<Rent*> rentAux;
    Vehicle* car;
    Ui::MainWindow *ui;
    CadastroVeiculoWindow* cadastro;
    EditarPerfilWindow* editar;
    MeusVeiculosWindow* veiculos;
    AlugarDialog* alugar;
};
#endif // MAINWINDOW_H
