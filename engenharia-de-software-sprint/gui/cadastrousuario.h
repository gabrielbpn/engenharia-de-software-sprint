#ifndef CADASTROUSUARIO_H
#define CADASTROUSUARIO_H

#include <QMainWindow>

QT_BEGIN_NAMESPACE
namespace Ui { class CadastroUsuario; }
QT_END_NAMESPACE

class CadastroUsuario : public QMainWindow
{
    Q_OBJECT

public:
    CadastroUsuario(QWidget *parent = nullptr);
    ~CadastroUsuario();

private:
    Ui::CadastroUsuario *ui;
};
#endif // CADASTROUSUARIO_H
