#ifndef ALUGARDIALOG_H
#define ALUGARDIALOG_H

#include <QDialog>
#include "../src/lib/system.h"

namespace Ui {
class AlugarDialog;
}

class AlugarDialog : public QDialog
{
    Q_OBJECT

public:

    explicit AlugarDialog(QWidget *parent = nullptr,Rent* rent=nullptr);
    ~AlugarDialog();

private slots:
    void on_calcularButton_clicked();

    void on_alugarButton_clicked();

private:
    Ui::AlugarDialog *ui;
    Rent* rentAux;
};

#endif // ALUGARDIALOG_H
