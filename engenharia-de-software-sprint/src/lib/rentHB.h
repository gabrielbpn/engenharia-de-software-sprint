#ifndef RENT_HB_H
#define RENT_HB_H


#include <cstdlib>
#include <iostream>
#include "handleBodySemDebug.h"
#include "rent.h"

using namespace std;

/**
*@brief Esta classe Fluxo conecta dois sistemas e por meio da equacao nele inserida trafere valores de um sistema ao outro
*
*@author Marcos Geraldo Braga Emiliano 1914012
*/
class RentBody : public Body {

protected:
  User* lessee;/* *< Esta variavel armazena o locatario  */
  User* locator;/* *< Esta variavel armazena a locacao */
  Vehicle *vehicle;/* *< Esta variavel armazena o veiculo */
  int status;/* *< Esta variavel armazena o status */
  double cost;/* *< Esta variavel armazena o custo */
  int duration;/* *< Esta variavel armazena a duracao que o veiculo ficara */

public:
/* *< Construtor*/
  RentBody();
  /* *< Apontador de user pego para o rentbody  */
  RentBody(User*);
/* *< Destrutor do tipo virtual */
  virtual ~RentBody();
/* *< Pega o valor de veiculo atraves do apontador vehicle*/
  Vehicle* getVehicle();
/* *< Altera o valor do veiculo*/
  void setVehicle(Vehicle*);
/* *< Pega o locatario*/
  User* getLessee();
/* *< Altera locatario*/
  void setLessee(User*);
/* *< Pega o locador*/
  User* getLocator();
/* *< Altera locador*/
  void setLocator(User*);
/* *< Pega o custo*/
  double getCost();
/* *< Altera o custo*/
  void setCost(double);
/* *< Pega o status*/
  int getStatus();
/* *< Altera o status*/
  void setStatus(int);
/* *< Pega a duracao*/
  int getDuration();
/* *< Altera a duracao*/
  void setDuration(int);

};


class RentHandle : public Rent, public Handle<RentBody>{
public:
  RentHandle(User* locator){
    pImpl_->setLocator(locator);
    pImpl_->setStatus(1);
  }

  RentHandle(){}

  ~RentHandle(){}

  virtual Vehicle* getVehicle(){return pImpl_->getVehicle();}

  virtual void setVehicle(Vehicle* vehicle){pImpl_->setVehicle(vehicle);}

  virtual User* getLessee(){return pImpl_->getLessee();}

  virtual void setLessee(User* lessee){pImpl_->setLessee(lessee);}

  virtual User* getLocator(){return pImpl_->getLocator();}

  virtual void setLocator(User* locator){pImpl_->setLocator(locator);}

  virtual double getCost(){return pImpl_->getCost();}

  virtual void setCost(double cost){pImpl_->setCost(cost);}

  virtual int getStatus(){return pImpl_->getStatus();}

  virtual void setStatus(int status){pImpl_->setStatus(status);}

  virtual int getDuration(){return pImpl_->getDuration();}

  virtual void setDuration(int duration){pImpl_->setDuration(duration);}

  //virtual void setLessee(User*){}

  //virtual void setLocator(User*){}

};
#endif
