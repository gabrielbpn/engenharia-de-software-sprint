#ifndef VEHICLE_HB_H
#define VEHICLE_HB_H

#include <cstdlib>
#include <iostream>
#include "vehicle.h"
#include "handleBodySemDebug.h"

using namespace std;


/**
*@brief A classe SistemaImpl é a classe que aramazena os valores, é a classe "conteiner"
*
*@author Marcos Geraldo Braga Emiliano 1914012
*/
class VehicleBody : public Body{
protected:
  string brand="";/* *<Esta variavel armazena marca */
  double price=0;/* *< Esta variavel armazena preco */
  string description="";/* *<Esta variavel armazena descricao*/
  string model="";/* *< Esta variavel armazena o modelo*/
  int year=0;/* *< Esta variavel armazenao ano  */
  string image=""; /* *< Esta variavel armazenao o caminho da imagem do veículo  */
public:
/* *< Construtor*/
  VehicleBody();
/* *< Destrutor*/
  ~VehicleBody();
/* *< Veiculo body passando parametro string*/
  VehicleBody(string);
/* *< Pega marca, do tipo void*/
  string getBrand(void);
/* *< Alteracao da marca*/
  void setBrand(string);
/* *< Pega o preco*/
  double getPrice();
/* *<Altera o preco */
  void setPrice(double);
/* *< Pega descricao*/
  string getDescription(void);
/* *< Alteracao descricao*/
  void setDescription(string);
/* *< Pega modelo*/
  string getModel(void);
/* *< Altera modelo*/
  void setModel(string);
/* *< Pega ano*/
  int getYear(void);
/* *< Altera ano*/
  void setYear(int);
/* *< Retorna o ano*/
  string getImage();
/* *< Altera o ano*/
  void setImage(string);
};
/* *< Criacao da classe veiculo handle, ligando em veiculo e handle com referencia de vehiclebody */
class VehicleHandle : public Vehicle, public Handle<VehicleBody>{
public:
    /* *< Veiculo handle passando os parametros model, marca, preco, descricao e ano do veiculo*/
    VehicleHandle(string model,string brand,double price,string description,int year){
      /* *< Alteracao de model */
      pImpl_->setModel(model);
      /* *< Alteracao de marca*/
      pImpl_->setBrand(brand);
      /* *< Alteracao de preco*/
      pImpl_->setPrice(price);
      /* *< Alteracao de descricao*/
      pImpl_->setDescription(description);
      /* *< Alteracao de ano*/
      pImpl_->setYear(year);
    }
//    /* *< Veiculo passando modelo do tipo string*/
//    VehicleHandle(string model){
//    /* *< Alteracao de modelo*/
//      pImpl_->setModel(model);
//    }
    /* *< Construtor*/
    VehicleHandle(){}
    /* *< Destrutor*/
    ~VehicleHandle(){}
    /* *< Pega marca e passa retorno de pImpl com a marca*/
    virtual string getBrand(){return pImpl_->getBrand();};
    /* *< Altera marca e pega a marca atraves de PImpl*/
    virtual void setBrand(string brand){pImpl_->setBrand(brand);};
    /* *< Pega o preco e passa retorno de pImpl com a marca*/
    virtual double getPrice(){return pImpl_->getPrice();};
    /* *< Altera preco e pega o preco atraves de PImpl*/
    virtual void setPrice(double price){pImpl_->setPrice(price);};
    /* *< Pega a descricao e passa retorno de Impl com a descricao*/
    virtual string getDescription(){return pImpl_->getDescription();};
    /* *< Altera a descricao e pega descricao atraves de Pimpl*/
    virtual void setDescription(string descrip){pImpl_->setDescription(descrip);};
    /* *< Pega modelo e passa retorno de pImpl com o modelo*/
    virtual string getModel(){return pImpl_->getModel();};
    /* *< Altera modelo e pega o modelo atraves de pImpl*/
    virtual void setModel(string model){pImpl_->setModel(model);};
    /* *< Pega o ano e passa retorno de pImpl com o ano*/
    virtual int getYear(){return pImpl_->getYear();};
    /* *< Altera o ano e pega o ano atraves de pImpl*/
    virtual void setYear(int year){pImpl_->setYear(year);};
    /* *< Pega o ano e passa retorno de pImpl com o ano*/
    virtual string getImage(){return pImpl_->getImage();};
    /* *< Altera o ano e pega o ano atraves de pImpl*/
    virtual void setImage(string img){pImpl_->setImage(img);};
};


#endif
