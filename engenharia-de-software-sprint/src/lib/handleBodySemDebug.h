/*!
 \file Bridge.h
 \brief The classes Handle and Body implements "bridge" design pattern (also known as
	"handle/body idiom").The class Body was implemented based on the class teCounted
	writed by Ricardo Cartaxo and Gilberto C�mara and founded in the geographic library TerraLib.
 \author Prof. Tiago Garcia de Senna Carneiro - UFOP, MG, Brazil
*/

#if ! defined( HANDLE_BODY )
#define HANDLE_BODY

/**
 * \brief
 *
 * The classes Handle and Body implements the "bridge" design pattern (also known as
 * "handle/body idiom").
 *
 */
template <class T>
class Handle
{

public:

	/// constructor
	Handle<T>( ){
		pImpl_ = new T;
        pImpl_->attach();
	}

	/// Destrutor
	virtual ~Handle<T>(){ pImpl_->detach(); 	}

	/// Copia do destrutor
	Handle<T>( const Handle& hd ):pImpl_( hd.pImpl_ ) { pImpl_->attach();  }

	/// Operador de assignment
	Handle<T>& operator=( const Handle& hd) {
		if (  this != &hd )
		{
			hd.pImpl_->attach();
			pImpl_->detach();
			pImpl_  = hd.pImpl_;
		}
		return *this;
	}
protected:

	/// referencia para a implementacao
	T *pImpl_;
};

/**
 * \brief
 *
 * The class Implementation was implemented based on the class teCounted writed by Ricardo Cartaxo
 * and Gilberto C�mara and founded in the geographic library TerraLib.
 */

class Body
{
public:
	/// Construtor: nenhuma referencia e passada quando o objeto esta sendo criado
	Body(): refCount_ ( 0 ){  }


	/// Incrementa o numero de referencia do objeto
	void attach ()	{ refCount_++; }

	/// Decrementa o numero de referencia para esse objeto
    /// Destroi se nao ha mais referencias
    void detach (){
		if ( --refCount_ == 0 )	{
			delete this;
		}
	}

	/// Retorna o numero de referencias para esse objeto
	int refCount(){ return refCount_; }

	/// Destrutor
	virtual ~Body(){}

private:

	/// Sem copia permitida
	Body(const Body&);

	/// Implementacao
	Body& operator=(const Body&){return *this;}

	int refCount_; 	/// O numero de referencias para esta classe

};

#endif
