#ifndef SYSTEM_HB_H
#define SYSTEM_HB_H

#include <vector>
#include <cstdlib>
#include <iostream>
#include "system.h"
#include "userHB.h"
#include "rentHB.h"
#include "vehicleHB.h"

using namespace std;



class SystemBody : public Body{

protected:
  string name="";/* *< Esta variavel aramazena o nome de um modelo */
  vector<User*> users;/* *< Esta variavel armazena um vector de variaves do tipo ponteiro para fluxo*/
  vector<Rent*> rents;/* *< Esta variavel armazena um vector de variaves do tipo ponteiro para fluxo*/
  static System* system;
public:
/* *< Construtor*/
  SystemBody();
/* *< Destrutor*/
  virtual ~SystemBody();
/* *< Passa como parametro uma string*/
  SystemBody(string);
/* *< Pega o nome*/
  string getName();
/* *< Altera o nome do tipo string*/
  void setName(string);
/* *< Adiciona usuario*/
  void add(User*);
/* *< Remove usuario*/
  void remove(User*);
/* *< Adiciona aluguel*/
  void add(Rent*);
/* *< Remove aluguel*/
  void remove(Rent*);
/* *< Criacao do sistema com o parametro nome do tipo string*/
  static System* criateSystem(string name);
/* *< Criacao do usuario do tipo string*/
  User* criateUser(string);
/* *< Criacao do aluguel*/
  Rent* criateRent(User*);
/* *< Criacao de veiculo, passando modelo, marca, preco, descricao e ano do mesmo*/
  Vehicle* criateVehicle(string model,string brand,double price,string description,int year);

  //Vehicle* criateVehicle(string model);

  User* getUser(string);
/* *< Vetor do tipo aluguel, aonde e passado do respectivo veiculo por um ponteiro, atraves de getRent*/
  vector<Rent*> getRent(string);
/* *<Vetor do tipo aluguel, aonde e passado do respectivo usuario por um ponteiro, atraves de getRent*/
  vector<Rent*> getRent(User*);
/* *< Indica a posicao que se encontrara o aluguel, do tipo inteiro*/
  vector<Rent*> getRent(int pos);
/* *< Adiciona sistema de forma estatica */
  static void add(System*);

private:
/* *< Sistema passando como referencia o operador com o endereco de systembody do tipo constante  */
  SystemBody& operator= (const SystemBody&);
/* *< Operador do tipo booleano que recebe sistema do tipo constante */
  bool operator== (const SystemBody&);
};
/* *< Classe sistema com ligacao a sistema e handle com referencia a systembody*/
class SystemHandle : public System, public Handle<SystemBody>{
public:
  /* *< Sistema handle com o parâmetro nome do tipo string*/
  SystemHandle(string name){
    /* *< Altera o nome*/
    pImpl_->setName(name);
  }
/* *< Sistema Handle*/
  SystemHandle(){}
/* *< Desconstrutor*/
  ~SystemHandle(){}
/* *< Pega o nome do tipo string virtual retornando o nome referenciado em pimpl*/
  virtual string getName(){return pImpl_->getName();}
/* *< Altera o nome do tipo string passando por pimpl*/
  virtual void setName(string name){pImpl_->setName(name);}
/* *< Adiciona usuario*/
  virtual void add(User* user){pImpl_->add(user);}
/* *< Remove usuariio*/
  virtual void remove(User* user){pImpl_->remove(user);}
/* *< Adiciona aluguel*/
  virtual void add(Rent* rent){pImpl_->add(rent);}
/* *< Remove aluguel*/
  virtual void remove(Rent* rent){pImpl_->remove(rent);}
/* *< Ponteiro de usuario, aonde ha a criacao de usuario do tipo string, retornando pImpl com o nome*/
  virtual User* criateUser(string name){return pImpl_->criateUser(name);}
/* *< Criacao de aluguel passando usuario e retornando pImpl com o usuario*/
  virtual Rent* criateRent(User* user){return pImpl_->criateRent(user);}
/* *< Ponteiro de veiculo, passando modelo, marca, preco descricao e ano do veiculo, retornando pImpl com os parametros*/
  virtual Vehicle* criateVehicle(string model,string brand,double price,string description,int year){return pImpl_->criateVehicle(model,brand,price,description,year);}

  //virtual Vehicle* criateVehicle(string model){return pImpl_->criateVehicle(model);}

  virtual User* getUser(string name){return pImpl_->getUser(name);}
/* *< Pega aluguel passando o veiculo retornando pImpl passando carro*/
  virtual vector<Rent*> getRent(string car){return pImpl_->getRent(car);}
/* *< Pega o aluguel passando usuario retornando pImpl usuario*/
  virtual vector<Rent*> getRent(User* user){return pImpl_->getRent(user);}
/* *< Pega aluguel passando a posicao do tipo inteiro, retornando pImpl passando a posicao*/
  virtual vector<Rent*> getRent(int pos){return pImpl_->getRent(pos);}
};

#endif
