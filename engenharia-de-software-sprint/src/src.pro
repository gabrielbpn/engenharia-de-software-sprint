QT -= gui

TEMPLATE = lib
DEFINES += SRC_LIBRARY

CONFIG += c++11

# You can make your code fail to compile if it uses deprecated APIs.
# In order to do so, uncomment the following line.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0

SOURCES += \
    lib/rentHB.cpp \
    lib/systemHB.cpp \
    lib/userHB.cpp \
    lib/vehicleHB.cpp


HEADERS += \
    lib/handleBodySemDebug.h \
    lib/rent.h \
    lib/rentHB.h \
    lib/system.h \
    lib/systemHB.h \
    lib/user.h \
    lib/userHB.h \
    lib/vehicle.h \
    lib/vehicleHB.h


# Default rules for deployment.
unix {
    target.path = /usr/lib
}
!isEmpty(target.path): INSTALLS += target
